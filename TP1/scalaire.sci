//example etant donne une matrice A carre de taille n et deux entiers l et c compris entre 1 et n calcule le produit escalaire entre la l'eme ligne de A et la c'eme cologne de A.
function [s]=scalaire(N,l, c)
    n=size(N);
    if (1<=l<=n) & (1<=c<=n) then         
       s=N(1:l)*N(1:c); 
    end
endfunction
