//division euclidienne example

function [quotient, reste]=divEucl(p,q)
    reste= p;
    quotient=0;
    while reste>=q
        reste=reste-q;
        quotient=quotient+1;
    end
endfunction
