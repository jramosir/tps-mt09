function [C]=pointfixe(g, Kmax, tol, x0)
    
    x=zeros(Kmax,1);
    x(1)=x0;
    
    for k=1:Kmax
        x(k+1)=g(x(k));
        
        if(abs(x(k+1)-x(k)))>tol
        error("pas de point fixe"); 
        else
    
        C=x;
        end
    end
    
    

endfunction
