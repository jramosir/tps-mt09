function [x]=resolchol(A,b)
    exec cholesky.sci;
    exec solinf.sci; 
    exec solsup.sci;
    
    C=cholesky(A);
    Ct=C';
    
    y=solinf(C,b);
    x=solsup(Ct,y);
    
endfunction
