function [x]=rich(a,b,c,u)
    
    [n,m]=size(a); 
    
    e=zeros(n,1);
    f=zeros(n,1);
    
    x=zeros(n,1);
    
    if b(1)==0 
        error("division par 0"); 
    else
        e(1)=-c(1)/b(1);
        f(1)=u(1)/b(1);
    end
    
    for i=2:(n-1)
        d=(a(i)*e(i-1))+b(i);
 
            e(i)=-c(i)/d;
            f(i)=(u(i)-(a(i)*f(i-1)))/d; 
        
    end
    
    d=(a(n)*e(n-1))+b(n);
    f(n)=(u(n)-(a(n)*f(n-1)))/d;  
    
    if(d==0)
            error("division par 0");
    else
            x(n)=f(n);
    end
    
    for i=(n-1):-1:1
        x(i)=(e(i)*x(i+1))+f(i); 
    end
    
endfunction
