function [d]= cald(T,y)
    
    [n,m]= size(T); 
    
    a=zeros(n,1);
    b=zeros(n,1);
    c=zeros(n,1);
    
    h=zeros(n,1);
    
    for i=1:(n-1)
        h(i)=T(i+1)-T(i);
    end
    
    b(1)=2/h(1);
    b(n)=2/h(n-1);
    for i=2:(n-1)
        b(i)=(2/h(i-1))+(2/h(i));
    end
    
    a(1)=0;
    for i=2:n
        a(i)=1/h(i-1);
    end
    
    c(n)=0
    for i=1:(n-1)
        c(i)=1/h(i);
    end
    
    u=zeros(n,1);
    u(1)=(y(2)-y(1))/(h(1)**2);
    u(n)=(y(n)-y(n-1))/(h(n-1)**2);
    for i=2:(n-1)
        u(i)=((y(i)-y(i-1))/(h(i-1)**2))+((y(i+1)-y(i))/(h(i)**2)); 
    end
    
    u=3*u; 
        
    d= rich(a,b,c,u);
endfunction
