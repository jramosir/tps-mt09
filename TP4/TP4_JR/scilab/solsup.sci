function [x]=solsup(U,b)
    
    n=length(b);
    
    if (size(U) ~= [n,n])
        error("matrice mon carrée")
    end
    
    x=zeros(n,1);
    
    for i=1:n
        if U(i,i)==0
            error("0 sur la diagonale")
        end
    end
    
    x(n)=b(n)/U(n,n);
    
    for i=n-1:-1:1
        somme=0;
        for j=i+1:n
            somme=somme+U(i,j)*x(j);
        end
        x(i)=(1/U(i,i))*(b(i)-somme);
    end

endfunction
