function [C]= cholesky(A)
    
    [n,m]=size(A);
    C=zeros(n,n);
    
    C(1,1) = sqrt(A(1,1));
    
    for i=2:n
        C(i,1) = A(i,1)/C(1,1)
    end
    
    for j=2:(n-1)
        sum=0;
        for k= 1:(j-1)
            sum=sum+(C(j,k)**2);
        end
        C(j,j)=sqrt(A(j,j)-sum);
        
        for i=(j+1):n
            sum2=0;
            for k= 1:(j-1)
                sum2=sum2+C(i,k)*C(j,k);
            end
            C(i,j)=(1/C(j,j))*(A(i,j)-sum2);
        end
    end
    
    sum3=0;
    for k=1:(n-1)
        sum3=sum3+(C(n,k)**2);
    end
    C(n,n)=sqrt(A(n,n)-sum3);
     
 
endfunction
