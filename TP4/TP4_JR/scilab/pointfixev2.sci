function [C,k]=pointfixe(g,Kmax,tol,x0)

    x=x0;

    for i=1:Kmax
        y = g(x);
        if ((norm(y-x)/norm(x))<tol)
            k = i;
            C = y;
            return
        else
            x = y;
        end
    end

    error("pas de convergence trouvée");
    
endfunction
