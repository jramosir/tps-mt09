//preparation
    T=[0; 1; 3 ;4]; 
    cc=[1 2 1; 4 1 -1; 1 -4 1]; 
    
    exec tracePrep.sci; 
    tracePrep(10, T,cc)
    tracePrep(200, T,cc)
//1
    A=[2 3 0 0 0; 1 3 1 0 0; 0 2 1 2 0; 0 0 1 1 1; 0 0 0 2 1];
    u=[-1; 0; 0; 3; 1];
    
    exec rich.sci; 
    [n,m]=size(A); 
    a=zeros(n,1); 
    b=zeros(n,1); 
    b=zeros(n,1); 
    
    for i=2:n
        a(i)=A(i,i-1); 
    end

    for i=1:(n-1)
        c(i)=A(i,i+1); 
    end
    
    for i=1:n
        b(i)=A(i,i); 
    end
    
    rich(a,b,c,u)
    

//2
    //a
    T=[1;3;4.5;5;6];
    
    exec place.sci; 
    place(T,-5)
    place(T,1.5)
    place(T,4.5)
    place(T,6)
    place(T,1)
    place(T,20)
    
    //b
    T=[1;3;4.5;5;6];
    cc=[1 0 1 0; 5 0 -(8/9) 0; 3 0 16 0; 7 0 -8 0]; 
    
    exec calcg.sci;
    calcg(3,T,cc)
    calcg(5,T,cc)

    //c
    T=[1;3;4.5;5;6];
    cc=[1 0 1 0; 5 0 -(8/9) 0; 3 0 16 0; 7 0 -8 0]; 
    
    exec trace.sci; 
    trace(10, T,cc)
    trace(200, T, cc)

//3
    //a
    T=[1; 3; 4.5; 5; 6];
    y=[1; 5; 3; 7; -1]; 
    
    exec cald.sci; 
    cald(T,y)
    
    //b
    T=[1; 3; 4.5; 5; 6];
    y=[1; 5; 3; 7; -1]; 
    
    exec calcoef.sci; 
    calcoef(T,y) 
    
//application
T=[1; 3; 4.5; 5; 6];
y=[1; 5; 3; 7; -1]; 

exec TracerCourbe.sci; 
exec rich.sci; 
TracerCourbe(T,y) 
    
