function [f]=trace(N,T,cc)

    exec calcg.sci;

    [n,m]=size(T);

    [t]=linspace(T(1), T(n),N);
    
    z=zeros(1,N);
    
    for i=1:N
        z(i)=calcg(t(i),T,cc);
    end
    
    
    f=plot(t,z);
    
    
endfunction

