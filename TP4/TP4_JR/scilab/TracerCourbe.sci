function [c] = TracerCourbe(T,y)
    exec calcoef.sci; 
    exec trace.sci;
    cc=calcoef(T,y); 
    N=200; 
    c=trace(N,T,cc); 
endfunction
