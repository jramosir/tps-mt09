function y= g(t,cc,T)

    exec place.sci;

    [n,m]=size(T);

    if(t<T(n))
        i=place(T,t);
        y=cc(i,1)+cc(i,2)*t+cc(i,3)*(t*t);
    else
        y=cc(n-1,1)+cc(n-1,2)*t+cc(n-1,3)*(t**2);
    end

endfunction
