function [i] = place(T,t)

    [n,m]= size(T);
    
    if(t<T(1))
        error("erreur, t inferieur a T[1]");
    end

    if(t>T(n))
        error("erreur, t upérieur a T[n]");
    end

    if(t==T(n))
        i=n-1;
    end

    imin=1;
    imax=n;

    while(imax-imin>1) do
        mil=floor((imax+imin)/2);
        if(t>=T(mil))
            imin=mil;
        else
            imax=mil;
        end
    end
    i=imin;

endfunction
