function [z]= calcg(t,T,cc)

    exec place.sci;

    [n,m]=size(T);

    if(t<T(n))
        i=place(T,t);
        z=cc(i,1)+cc(i,2)*(t-T(i))+cc(i,3)*((t-T(i))**2)+cc(i,4)*((t-T(i))**2)*(t-T(i+1));
    else
        z=cc(n-1,1)+cc(n-1,2)*(t-T(n-1))+cc(n-1,3)*((t-T(n-1))**2)+cc(n-1,4)*((t-T(n-1))**2)*(t-T(n)); 
    end

endfunction
