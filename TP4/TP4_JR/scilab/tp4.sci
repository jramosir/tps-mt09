//EXERCICE 1

    //a
    A1=[15 10 18 12; 10 15 7 13; 18 7 27 7; 12 13 7 22];
    exec cholesky.sci; 
    C = cholesky(A1);
    C
    /*C est triangulaire inférieur et tous les éléments diagonaux sont positifs
    A1 admet une factorisation de Chloesky, elle est donc définie positive. */

    //b
    A1=[15 10 18 12; 10 15 7 13; 18 7 27 7; 12 13 7 22];
    b1=[53; 72; 26; 97];
    exec resolchol.sci
    resolchol(A1,b1)
    
    
//EXERCICE 2

    //a
    exec hilbertmat.sci;
    n=4;
    H=hilbertmat(n);
    H

    //b
    vp=spec(H);
    /*H est symetrique, son conditionnement en norme 2 est donc égal 
    a la valeur absolue de sa vp maximum divisé par la valeur absolue de sa vp minimale*/
    conditionnement = abs(max(vp)) / abs(min(vp));
    conditionnement

    //c
    cond(H)
    /*conditionnement = cond(H)
    la fonction cond de scilab donne le conditionnement en norme 2 
    d'une matrice H)*/

    //d
    H4=hilbertmat(4);
    b=[1; 1; 1; 1];
    exec resolchol.sci;
    x=resolchol(H4,b);
    x
    
    H4=hilbertmat(4);
    btilde=[0.999; 1.004; 0.995; 1.005];
    exec resolchol.sci;
    xtilde=resolchol(H4,btilde);
    xtilde
    
    ecRelb = norm(btilde)/norm(b);
    ecRelb
    
    ecRelx=norm(xtilde)/norm(x);
    ecRelx
    
    H4\b
    H4\btilde
    
    //e
    H10=hilbertmat(10);
    b=[1;1;1;1;1;1;1;1;1;1];
    exec resolchol.sci;
    x=resolchol(H10,b);
    x

    H10=hilbertmat(10);
    r=rand(10,1)*(1e-3); 
    btilde=b+r
    exec resolchol.sci;
    xtilde=resolchol(H10,btilde);
    xtilde
    
    ecRelb = norm(btilde)/norm(b);
    ecRelb
    
    ecRelx=norm(xtilde)/norm(x);
    ecRelx
    
    H10\b
    H10\btilde

    /*on remarque des erreurs très petites entre les solutions avec \ et celles avec resolchol     */
    
//EXERCICE 3

    //a
    exec pointfixev2.sci;
    //b
    //exec pointfixe.sci;
    exec g0.sci; 
    ptFixe = pointfixe(g0,100,%eps,1);
    x=0:0.2:2;
    plot(x,g0(x));
    plot(x,x);
    plot(x,ptFixe,"r")
    
    //c
    exec g1.sci; 
    ptFixe2 = pointfixe(g1,100,%eps,2); 
    ptFixeMoins2 = pointfixe(g1,100,%eps,-2); 
    ptFixe5 = pointfixe(g1,100,%eps,5); 
    ptFixe10 = pointfixe(g1,100,%eps,10); 
    
    x=-4:0.1:4;
    plot(x,g1);
    plot(x,x);
    plot(x,ptFixe2,"r")
    plot(x,ptFixeMoins2,"r")
    
    //d
    //exec pointfixe.sci;
    exec g2.sci; 
    ptFixe1 = pointfixe(g2,100,%eps,1); 
    ptFixe2 = pointfixe(g2,100,%eps,2); 
    ptFixe5 = pointfixe(g2,100,%eps,5); 
    ptFixe10 = pointfixe(g2,100,%eps,10); 
    
    x=-5:0.1:5;
    plot(x,g2);
    plot(x,x);
    plot(x,ptFixe1,"r")
    plot(x,ptFixe2,"r")
    
    //e
    exec pointfixe.sci;
    exec g3.sci; 
    ptFixeMoins1 = pointfixe(g3,100,%eps,-1); 
    ptFixe2 = pointfixe(g3,100,%eps,2); 
    ptFixe5 = pointfixe(g3,100,%eps,5); 
    ptFixe10 = pointfixe(g3,100,%eps,10); 
    
    x=-5:0.1:5;
    plot(x,g3);
    plot(x,x);
    plot(x,ptFixeMoins1,"r")
    plot(x,ptFixe2,"r")
    
    
    
    
    
    
    
    
    
    
    
    
    
