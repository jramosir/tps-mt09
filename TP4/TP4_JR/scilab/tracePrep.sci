function [f]=tracePrep(N,T,cc)

    exec g.sci;

    [n,m]=size(T);

    [t]=linspace(T(1), T(n),N);
    
    z=zeros(1,N);
    
    for i=1:N
        z(i)=g(t(i),cc,T);
    end
    
    
    plot(t,z);
    
    
endfunction
