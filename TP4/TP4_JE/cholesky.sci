function [C] = cholesky(A)
    //initialization de la matriz C avec la taille de A
    [n,m]=size(A);
    C = zeros(n,m);
    
    //premier élément
    C(1,1)=sqrt(A(1,1));
    
    //première colonne commencant par le deuxieme element
    for i=2:n
      if abs(C(j,j))<%eps
         error("Attention : Dénominateur nul");
      end
      C(i,1) = A(i,1)/C(1,1); 
    end
    
    for j=2:m
        X = 0; 
        //for k=1:j-1
        //   X = X + C(j,k)*C(j,k);
        //end
        //forme de calculer la somme pour les termes A(j,j)
        //dans le meme boucle:
        X = C(j,1:j-1)*C(j,1:j-1)'
        if (abs(A(j,j)-X)<=%eps)
          error("Attention : Dénominateur nul");
        end
        C(j,j)=sqrt(A(j,j)-X);
        for i=3:n
            Y = 0;
            //for k=1:j-1
              //  Y = Y + C(i,k)*C(j,k);
           // end
            Y = C(i,1:j-1)*C(j,1:j-1)'
            C(i,j)=(A(j,i)-Y)/C(j,j);
        end
    end
    
endfunction
