function [y]=g1(x)
    a = 4;
    if (abs(x)<=%eps) 
        error("Dénominateur nul !");
    end
    y = a/x;
endfunction
