function [x] = resolchol(A,b)
    C = cholesky(A);
    y = solinf2(C,b);
    C = C';
    x = solsup2(C,y);
endfunction
