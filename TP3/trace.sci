function trac(N,T,cc)
    [n m]=size(T)
    t=linspace(T(1),T(n),N)
    for j=1:N
        [i]=place(T,t(j))
        if t(j)==T(n) then
            g(j)=cc(n-1,1)+(t(j)*cc(n-1,2))+((t(j)^2)*cc(n-1,3))
        else
            g(j)=cc(i,1)+(t(j)*cc(i,2))+(t(j)*t(j)*cc(i,3))
        end
    end
    plot(g);
endfunction
