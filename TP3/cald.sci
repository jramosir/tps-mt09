function [d]=cald(T,y)
    [n m]=size(T)
    for i=1:n-1
        h(i)=T(i+1)-T(i)
    end
    //diagonal b
    b=zeros(n,1)
    b(1)=2/h(1)
    for i=2:n-1
        b(i)=2/h(i-1) + 2/h(i)
    end
    b(n)=2/h(n-1)
    
    //diagonal a et c
    a=zeros(n-1,1)
    c=zeros(n-1,1)
    for i=1:n-1
        a(i)=1/h(i)
        c(i)=1/h(i)
    end
    //matriz A
    A=diag(b) + diag(a,-1) + diag(c,1)
    
    //vecteur u
    u=zeros(n,1)
    u(1)=(y(2)-y(1))/(h(1)^2)
    for i=2:n-1
        u(i)=((y(i)-y(i-1))/(h(i-1)^2)) + ((y(i+1)-y(i))/(h(i)^2))
    end
    u(n)=(y(n)-y(n-1))/(h(n-1)^2)
    u=3*u
    d=rich(a,b,c,u)
endfunction
