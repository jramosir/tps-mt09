function [i]=place(T,t)
    [n m]=size(T)
    if t<T(1)|t>T(n) then
        disp("t out of bounds => i=-1", t)
        i=-1;
        return
        //error('t out of bounds');
    end
    imin=1
    imax=n;
    while (imax-imin)>1
        mil=floor((imax+imin)/2)
        if t>=T(mil) then
            imin=mil;
        else
            imax=mil;
        end
    end
    i=imin;
endfunction
