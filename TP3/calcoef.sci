function [cc]=calcoef(T,y)
    [n m]=size(T)
    cc=zeros(n-1,4)
    for i=1:n-1
        h(i)=T(i+1)-T(i)
    end
    for i=1:n-1
        cc(i,1)=y(i)
    end    
    d=cald(T,y)
    for i=1:n-1
        cc(i,2)=d(i)
    end 
    for i=1:n-1
        cc(i,3)=(y(i+1)-y(i))/(h(i)^2)-(d(i)/h(i))
    end
    for i=1:n-1
        cc(i,4)=(d(i+1)+d(i))/(h(i)^2)-(2*(y(i+1)-y(i))/(h(i)^3))    
    end
endfunction



