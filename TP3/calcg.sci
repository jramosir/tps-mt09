function [z]=calcg(t,T,cc)
    [n m]=size(T)
    i=place(T,t)
    if t==T(n) then
        z=cc(n-1,1)+((t-T(n-1))*cc(n-1,2))+(((t-T(n-1))^2)*cc(n-1,3))+((((t-T(n-1))^2)*(t-T(n)))*cc(n-1,4))
    else
        z=cc(i,1)+((t-T(i))*cc(i,2))+(((t-T(i))^2)*cc(i,3))+((((t-T(i))^2)*(t-T(i+1)))*cc(i,4))
        end
 
endfunction
