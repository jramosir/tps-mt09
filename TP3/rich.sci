function [x]=rich(a,b,c,u)
    [n m]=size(b)
    x= zeros(n,1)
    e(1)=-c(1)/b(1)
    f(1)=u(1)/b(1)
    for i=2:n
        d=(a(i-1)*e(i-1))+b(i)
        if abs(d) < 1.d-16 then
            error('d=0')
        end
        if(i~=n)then
            e(i)=-c(i)/d //e(n) pas compris
        end
        f(i)=(u(i)-(a(i-1)*f(i-1)))/d
    end
    x(n)=f(n)
    for i=n-1:-1:1
        x(i)=(e(i)*x(i+1))+f(i)
    end
endfunction
