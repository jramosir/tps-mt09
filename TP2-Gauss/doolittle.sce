A = [[2 1 -4] ; [3 3 -5] ; [4 5 -2]]
B = [8 ; 14 ; 16]

function x = doolittle(A, B)
	[n m] = size(A)
	[p q] = size(B)
	//test A est carre
	if n ~= m then 
		error('A est non carre')
	end
	//test B est colonne
	if q ~= 1 then 
		error('B doit etre colonne')
	end
	//test A et B ont meme nombre de lignes
	if p ~= n then
		error('A et B doit avoir meme nombre de lignes')
	end
    L = eye(n,n)
    U= zeros(n,n)
    for k=1:n
        for j=k:n
            U(k,j)= A(k,j) - sum(L(k,1:k-1)) * U(p,j)
        end
        if abs(U(k,k)) < 1.d-16 then
				error('pivot nul')
		end
        for i=k+1:n
            L(i,k)=1/U(k,k)*(A(i,k) - sum(L(i,1:k-1)) * U(p,k))
        end
    end
    
endfunction
x=doolittle(A,B)
x2=A\B
