function [x]=solinf(L, b)
    [n m] = size(L)
	[p q] = size(b)
//test L est carre
	if n ~= m then 
		error('L est non carre')
	end
	//test b est colonne
	if q ~= 1 then 
		error('b doit etre colonne')
	end
	//test A et b ont meme nombre de lignes
	if p ~= n then
		error('A et B doit avoir meme nombre de lignes')
	end
    // remontee par substitution et calcul de la solution x
    x=zeros(n,1)
    for i=1:n
        if abs(L(i,i)) < 1.d-16 then
            error('A non inversible')
        end
        s=0
        for j=1:i-1
            //Calcul de la somme des termes connus (1er ligne toujours connu)
            s= s + L(i,j) * x(j)
        end 
        //affectation au x du valeur connu depuis s et x deja connus
        x(i)=(b(i)-s)/L(i,i)
    end

endfunction
