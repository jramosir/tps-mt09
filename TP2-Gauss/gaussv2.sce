A = [[2 1 -4] ; [3 3 -5] ; [4 5 -2]]
B = [8 ; 14 ; 16]

function x = gauss(A, B)
	[n m] = size(A)
	[p q] = size(B)
	//test A est carre
	if n ~= m then 
		error('A est non carre')
	end
	//test B est colonne
	if q ~= 1 then 
		error('B doit etre colonne')
	end
	//test A et B ont meme nombre de lignes
	if p ~= n then
		error('A et B doit avoir meme nombre de lignes')
	end
	//Echelonage de la matrice A
	//copie de B dans la m+1 eme colonne de A
	A(:,m+1) = B
	//boucle sur les colonnes
	for j = 1:n-1
		//boucle sur les lignes sous la diagonale
		for i = j+1:n
			//test por verifier que le pivot n'est pas nul
			if abs(A(j,j)) < 1.d-16
				error('pivot nul')
			end
			A(i, :) = A(i,:) -A(i,j)/A(j,j)*A(j,:)
		end
	end
	// remontee par substitution et calcul de la solution x
	x = zeros(size(n,1))
	//xi = (bi - sum(j=i+1 ^n) aij * j) /aii
	for i =1:n
	 A(i,:) = A(i,:) / A(i,i)
	end
	//bi = A(i,n+1) donc la formule devient:
	//xi = A(i,n+1) - sum(j=1^n)aij*j
	x(n) = A(n,n+1)
    for i=n-1:-1:1
		x(i) = A(i,n+1) - sum(A(i,i+1:n).*x(i+1:n)')
	end

endfunction

x= gauss(A,B)
x2 = A\B
