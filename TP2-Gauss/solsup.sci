function [x]=solsup(U, b)
    [n m] = size(U)
	[p q] = size(b)
//test L est carre
	if n ~= m then 
		error('U est non carre')
	end
	//test b est colonne
	if q ~= 1 then 
		error('b doit etre colonne')
	end
	//test A et b ont meme nombre de lignes
	if p ~= n then
		error('U et b doit avoir meme nombre de lignes')
	end
    // remontee par substitution et calcul de la solution x (commencant en n)
    x=zeros(n,1)
    for i=n:-1:1 //ne passe plus pour n
        if abs(U(i,i)) < 1.d-16 then
            error('A non inversible')
        end
        s=0
        for j=n:-1:i
            //Calcul de la somme des termes connus (neme ligne toujours connu)
            s= s + U(i,j) * x(j)
        end 
        //affectation au x du valeur connu depuis s et x deja connus
        x(i)=(b(i)-s)/U(i,i)
    end

endfunction
