function [U,e]=trisup(A,b)
    [n m] = size(A)
	[p q] = size(b)
//test L est carre
	if n ~= m then 
		error('A est non carre')
	end
	//test b est colonne
	if q ~= 1 then 
		error('b doit etre colonne')
	end
	//test A et b ont meme nombre de lignes
	if p ~= n then
		error('A et b doit avoir meme nombre de lignes')
	end
for k=1:n-1
    if abs(A(k,k)) < 1.d-16 then
            error('A pivot nul')
    end
    for i=k+1:n
        c=A(i,k)/A(k,k)
        b(i)= b(i) - c*b(k)
        A(i,k) = 0
        for j=k+1:n
            A(i,j) = A(i,j) - c *A(k,j)
        end
    end  
end
if abs(A(n,n)) < 1.d-16 then
            error('A pivot nul')
    end
U=A
e=b
endfunction
