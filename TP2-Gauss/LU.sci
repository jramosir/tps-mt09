function [L,U]=LU(A)
    [n m] = size(A)
	//test A est carre
	if n ~= m then 
		error('A est non carre')
	end
    L = eye(n,n)
    U= zeros(n,n)
    for k=1:n-1
    if abs(A(k,k)) < 1.d-16 then
            error('A pivot nul')
    end
    for i=k+1:n
        c=A(i,k)/A(k,k)
        A(i,k) = 0
        L(i,k) = c
        for j=k+1:n
            A(i,j) = A(i,j) - c *A(k,j)
        end
    end  
end
if abs(A(n,n)) < 1.d-16 then
            error('A pivot nul')
    end
U=A
endfunction
