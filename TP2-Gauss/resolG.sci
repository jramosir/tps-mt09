function [x]=resolG(A,b)
    exec trisup.sci
    exec solsup.sci
    [U,e]=trisup(A,b)
    [x]=solsup(U,e)
endfunction
