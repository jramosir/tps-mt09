//preparation du tp
exec "constrply.sci"
exec "mcnorm.sci"
exec "g1.sci"
tau=[0;1;2]
y=[1;3;7]
A=constrply(tau)
x=mcnorm(A,y)
f=A*x //egal au vecteur y car x=1
//on trouve que la courbe que mieux s'approche est x0+x1t+x2t^2
//=>1+t+t^2
p=linspace(0,2,20)'
g=g1(x,p); //avec g1 evalutation dun polynome °2 avec t=p
plot(p,g) //parabole des valeurs qui approchent la fonction
plot(tau,y,"r") //graphique des valeurs donnes.

//Aplication2
tau2=linspace(0,2,9)'
y2 = [1, 1.7, 1.95, 1.8, 3., 3.6, 4.45, 5.9, 6.6]'
A2=constrply(tau2)
x2=mcnorm(A2,y2)
p2=linspace(0,2,20)'
g2=g1(x2,p2)
plot(p2,g2)
plot(tau2,y2,"+r")
/*
p2=linspace(0,2,50)'
g2=g1(x2,p2); //g1: fonction 1+x+x^2
plot(p2,g2) //parabole des valeurs qui approchent la fonction
*/


//******************TP6*******************************************
exec "construct.sci"
exec "mcnorm2.sci"
exec "mcQR.sci"
//aplication1
t = [0, 1, 3, 4, 5.5, 6]'
tau = t
A=construct(t,tau)
x=mcnorm2(A)
//les valeurs de tau etant egaux a t on aura t-ti/ti+1-ti = 0 pour tt

//aplication2
t = [0, 1, 3, 4, 5.5, 6]'
tau = [0.5, 2, 2.5, 3.5, 4.5, 5.75]'
y = [1, 1.5, 1.25, 0, 0, 1.5]'
A2=construct(t,tau) //
z=mcnorm2(A2,y)
g=A2*z

plot(tau(1:length(tau)),y(1:length(y)),"+r")
plot(tau,g,'-')

//aplication3
t = [0, 1, 3, 4, 5.5, 6]
tau=[0:0.3:6]'
y = [0, 0.6, 1.4, 1.7, 2.1, 1.9, 1.6, 1.4, 1.4, 1, 0.5, 0.4, -0.2, -0.8, -0.5, 0, 0.4, 1, 1.6, 1.7, 1.2]'
A3=construct(t,tau)
z=mcnorm2(A3,y)
g=A3*z
plot(tau(1:length(tau)),y(1:length(y)),"+r")
plot(tau,g) //plus de casures que necessaires???
plot(t,z,'-') //

cond(A3'*A3) //M=A'*A
//

//EX3

exec "newton.sci"
exec "courbe.sci"
exec "foncjac.sci"
N=50
courbe(N);

x0=[4;3];
tol=0.00001;
N=20;
[x,k]=newton(foncjac,tol,N,x0)
plot(x(1),x(2),"+r");

x0=[-10;4]
[x,k]=newton(foncjac,tol,N,x0)
plot(x(1),x(2),"+r");
//Ex4.
//a)
/*
A=Q*R 
Q' = 1/Q  => Q*Q' = I //car Q ortonormal 
=> Q'*A=R avec R tri sup
A*x=b  <=> Q*R*x=b //on multiplie par Q'
R*x=Q'*b
*/
exec "mcQR.sci"
zq=mcQR(A3,y)
[Q,R]=qr(A3)
cond(A3'A3)
cond(R)
