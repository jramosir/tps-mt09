function A=constrply(tau)
    [m,p] = size(tau)
    for n=1:m
        A(n,1) = 1
        for i=2:3
        A(n,i)=tau(n)^(i-1)
        end
    end
endfunction
