function x=mcnorm2(A,y)
    exec "resolchol.sci"
    x=resolchol(A'*A,A'*y)
endfunction
