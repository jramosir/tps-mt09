function []=courbe(N)
    x=linspace(-3,3,N)
    
    plot(x,exp(x))
    plot(x,sqrt(4-(x^2)))    
endfunction
