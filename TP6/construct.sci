function [A]=construct(t, r)
    exec "place.sci"
    m = length(r)
    n = length(t)
    A = zeros(m, n)   
    for i = 1 : m       
            s=place(t,r(i))
            a = ((r(i) - t(s+1))/(t(s) - t(s+1)))
            b = ((r(i) - t(s))/(t(s+1) - t(s)))
            A(i, s)= a
            A(i, s+1)= b
    end
endfunction

/*
    a = ((t(1) - r(2))/(r(1) - r(2)))
    b = ((t(1) - r(1))/(r(2) - r(1)))
    A(1,1)= a
    A(1,2)= b
    
    a = ((t(2) - r(2))/(r(1) - r(2)))
    b = ((t(2) - r(1))/(r(2) - r(1)))
    A(2,2)= a
    A(2,3)= b
    
    a = ((t(3) - r(4))/(r(3) - r(4)))
    b = ((t(3) - r(3))/(r(4) - r(3)))
    A(3,3)= a
    A(3,4)= b
    
    a = ((t(4) - r(5))/(r(4) - r(5)))
    b = ((t(4) - r(4))/(r(5) - r(4)))
    A(4,4)= a
    A(4,5)= b
    
    a = ((t(5) - r(6))/(r(5) - r(6)))
    b = ((t(5) - r(5))/(r(6) - r(5)))
    A(5,5)= a
    A(5,6)= b
    
    a = ((t(6) - r(2))/(r(1) - r(2)))
    b = ((t(6) - r(1))/(r(2) - r(1)))
    A(6,5)= a
    A(6,6)= b
*/
