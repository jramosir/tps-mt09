function [x]=solinf(L,b)
    
    n=length(b);
    
    if (size(L) ~= [n,n])
        error("matrice mon carrée")
    end
    
    x=zeros(n,1);
    
    for i=1:n
        if L(i,i)==0
            error("0 sur la diagonale")
        end
    end
    
    for i=1:n
        somme=0;
        if i>1
            for j=1:i-1
                somme=somme+L(i,j)*x(j);
            end
        end
        x(i)=(1/L(i,i))*(b(i)-somme);
    end

endfunction
