//Exercice 4 TP6

/*
a)
A=Q*R 
Q' = 1/Q  => Q*Q' = I //car Q ortogonale 
=> Q'*A=R avec R la matrice par bloc compose par R° tri sup et 0
A*x=b  <=> Q*R*x=b //on multiplie par Q'
R*x=Q'*b
on s'interesse au solution de R°x=c ou R° est la matrice tri sup et c est le vecteur compose de n premier elements de b
*/
//b) mcQR.sci
//c)
exec "construct.sci"
exec "mcnorm2.sci"
exec "mcQR.sci"
t = [0, 1, 3, 4, 5.5, 6]
tau=[0:0.3:6]'
y = [0, 0.6, 1.4, 1.7, 2.1, 1.9, 1.6, 1.4, 1.4, 1, 0.5, 0.4, -0.2, -0.8, -0.5, 0, 0.4, 1, 1.6, 1.7, 1.2]'
A3=construct(t,tau)
z=mcnorm2(A3,y)
g=A3*z

zq=mcQR(A3,y)
gq=A3*zq

//gq et g sont egaux donc la solution approximé est la meme
[Q,R]=qr(A3)
cond(A3'A3) //4.3067807
cond(R) //2.0752785
//le conditionement de R est la moitie du M, ce qui nous dit que R est plus stable et qu'une petite perturbation en M donnerai des grandes perturbations dans la solution.

