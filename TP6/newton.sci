function [x,k]=newton(foncjac, tol, N, x0)
    exec foncjac.sci; 
    [Fx0,Jx0]=foncjac(x0);
    [Fx,Jx]=foncjac(x0);
    
    x=x0;
    k=0;
    while k < N
        k=k+1;
        
        h=-(Jx\Fx); 
        x=x+h;
        
        [Fx,Jx]=foncjac(x);
        
        if((norm(Fx)/norm(Fx0))<tol)
           x=return(x);
        end
    end
    
    error("Newton na pas convergé")
    
endfunction
