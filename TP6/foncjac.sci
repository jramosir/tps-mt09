function [F,J]=foncjac(x)
    F=zeros(2,1)
    F(1)=(x(1)^2)+(x(2)^2)-1;
    F(2)=x(2)-exp(x(1));
    
    J=zeros(2,2)
    J(1,1)=2*x(1)
    J(1,2)=2*x(2)
    J(2,1)=-x(2)^exp(x(1));
    J(2,2)=1
    
endfunction
