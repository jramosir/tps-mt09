function [TV,TE]=compar(a,t0,T)
    exec "vdpexacte.sci"
    exec "5_f.sci"
    exec "pointmilieu.sci"
    exec "eulerexpli.sci"
    exec "rk4.sci"
    TV=zeros(4,3)
    TE=zeros(4,3)
    N=[10,100,1000,10000]'
    sol=vdpexacte(t0+T)
    for i=1:4       
        ze = eulerexpli(a,t0,T,N(i),f)
        zp=pointmilieu(a,t0,T,N(i),f)
        zr = RK4(a,t0,T,N(i),f)
        TV(i,1)=ze(N(i)+1)
        TV(i,2)=zp(N(i)+1)
        TV(i,3)=zr(N(i)+1)
    end
    TE=abs(sol-TV)
endfunction
