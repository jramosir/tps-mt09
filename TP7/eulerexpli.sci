function [z]=eulerexpli(a,t0,T,N,f)
    h=T/N
    tn=t0
    p=length(a)
    z=zeros(p,N+1)
    z(1:p,1)=a
    y=a
    for i=1:N
        y=y+(h*f(tn,y))
        z(1:p,i+1)=y
        tn=tn+h
    end
endfunction
