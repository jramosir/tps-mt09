exec "eulerexpl.sci"
exec "equation.sci"
exec "solution.sci"
//eulerexpl(f,y0,t0,T,N)
//N=10
[z1]=eulerexpl(equation,0,0,5,10)
t1=linspace(0,5,11)
for i=1:11
    sol1(i)=solution(t1(i))
end
plot(t1,z1,"+b")
plot(t1,sol1',"r")
//N=20
[z2]=eulerexpl(equation,0,0,5,20)
t2=linspace(0,5,21)
for i=1:21
    sol2(i)=solution(t2(i))
end
    plot(t2,z2,"+g")
    plot(t2,sol2',"r")
    
//N=50
[z3]=eulerexpl(equation,0,0,5,50)
t3=linspace(0,5,51)
//v=ode(0,0,t3,equation) //verification function ode
for i=1:51
    sol3(i)=solution(t3(i))
end
    plot(t3,z3,"+b")
    plot(t3,sol3',"r")
