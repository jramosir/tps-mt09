function [Z]=pointmilieu(a,t0,T,N,f)
    h=T/N
    tn=t0
    p=length(a)
    Z=zeros(p,N+1)
    Z(1:p,1)=a
    y=a
    for i=1:N
        y=y+(h*f(tn+(0.5*h),y+(0.5*h*f(tn,y))))
        Z(1:p,i+1)=y
        tn=tn+h
    end
endfunction
