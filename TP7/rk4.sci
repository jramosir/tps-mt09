function Z=RK4(a,t0,T,N,f)
    h=T/N
    tn=t0
    p=length(a)
    Z=zeros(p,N+1)
    Z(1:p,1)=a
    y=a
    for i=1:N
        k0=f(tn,y)
        k1=f(tn+(h/2),y+((h/2)*k0))
        k2=f(tn+(h/2),y+((h/2)*k1))
        k3=f(tn+h,y+(h*k2))
        y=y+((h/6)*(k0+(2*k1)+(2*k2)+k3))
        Z(1:p,i+1)=y
        tn=tn+h
    end
endfunction
