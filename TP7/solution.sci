function s=solution(t)
    s=2*exp(t)-(t^2)-(2*t)-2
endfunction
