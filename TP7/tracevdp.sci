function tracevdp(a,t0,T,Neul,Nptmil,Nrk4,Node)
    exec "vdp.sci"
    exec "pointmilieu.sci"
    exec "eulerexpli.sci"
    exec "rk4.sci"
    
    //schema ordre 2 plus precise que euler ordre 1
    //utilise 2n la fonction f(t,X).
    
    zp=pointmilieu(a,t0,T,Nptmil,vdp)
    [n,m]=size(zp)
    xp1=zp(1,1:m) //1ere ligne complete de zp
    xp2=zp(2,1:m)
    //plot(xp1,xp2,"b-")
    
    ze = eulerexpli(a,t0,T,Neul,vdp)
    [n,m]=size(ze)
    xe1=ze(1,1:m)
    xe2=ze(2,1:m)
    plot(xe1,xe2, "g")
    
    te=linspace(t0,T,Node+1)
    zd=ode(a,t0,te,vdp)
    xd1=zd(1,1:m)
    xd2=zd(2,1:m)
    //plot(xd1,xd2, "r")
    
    zr = RK4(a,t0,T,Nrk4,vdp)
    [n,m]=size(zr)
    xr1=zr(1,1:m)
    xr2=zr(2,1:m)
    plot(xr1,xr2, "m*")
    
    
endfunction
