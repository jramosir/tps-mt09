clear
clc
exec "tracevdp.sci"
exec "compar.sci"
a=[2;-2]
t0=0
T=15
Np=100
Ne=100
Nd=100
Nr=100
/*tracevdp(a,t0,T,Neul,Nptmil,Nrk4,Node)
  b : point milieu
  g : euler explicite
  r : ode (scilab) 
  m : rk4
  changer colours et commenter plot dans tracevdp
*/
//tracevdp(a,t0,T,Ne,Np,Nr,Nd)
/*pour que chaque methode ait le meme nombre d'appels de f on
  trouve que np=1/2nrk. la precision de rk sera moins precis car   on fait la moitie d'iterations que point milieu.
*/
a=1
[TV,TE]=compar(a,t0,T)
ere=-log10(TE(:,1))'
erp=-log10(TE(:,2))'
err=-log10(TE(:,3))'
N=[10,100,1000,10000]
N=log10(N)
plot(N,ere,"g")    
plot(N,erp,"b")
plot(N,err,"r")
m1=reglin(N,ere)
m2=reglin(N,erp)
m3=reglin(N,err)

//la relacion entre el error de cada metodo y el numero de iteraciones esta dada por la pendiente de la recta, se observa que entre mas preciso es el metodo requiere menos iteraciones para acercarse precisamente a la solucion
