function x=vdpexacte(t)
    x=((3/2)*exp(-t)) - ((1/2)*cos(t)) + ((1/2)*sin(t))
endfunction
