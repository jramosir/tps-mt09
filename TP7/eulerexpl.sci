function [z]=eulerexpl(f,y0,t0,T,N)
    h=T/N
    tn=t0
    p=length(y0)
    z=zeros(p,N+1)
    z(1:p,1)=y0
    y=y0
    for i=1:N
        y=y+(h*f(tn,y))
        z(1:p,i+1)=y
        tn=tn+h
    end
endfunction
